var list = {
    init: function () {
        list.getPersons();
    },
    persons: null,
    index: 0,
    personsCounter: 1,
    totalPersonsNumber: null,
    getPersons: function () {
        $.ajax({
            type: "GET",
            url: "files/data/list.json",
            dataType: 'json'
        })
                .fail(function () {
                    alert("Nie mogę załadować danych");
                })
                .done(function (json) {
                    list.persons = json[list.index];
                    list.totalPersonsNumber = json.length;
                    list.createList();
                    list.counter();


                    $('.right').on('click', function () {
                        $('.left').prop("disabled", false);
                        list.index++;
                        if (list.index+1 === list.totalPersonsNumber) {
                            $(this).prop("disabled", true);
                        }
                        list.personsCounter++;
                        list.persons = json[list.index];
                        list.createList();
                        list.counter();
                    });

                    $('.left').on('click', function () {
                        $('.right').prop("disabled", false);
                        if (list.index === 1) {
                            $(this).prop("disabled", true);
                        }
                        list.index--;
                        list.personsCounter--;
                        list.persons = json[list.index];
                        list.createList();
                        list.counter();
                    });

                    $('.detailsButton').on('click', function () {
                        var detailButton = $('.detailInfo');
                        detailButton.slideToggle(300);
                    });
                });
    },
    createList: function () {
        var mainTable = $("table.mainTable");
        mainTable.empty();
        mainTable.append("<tr><td><strong>Imię:</strong></td><td>" + list.persons.name + "</td></tr>");
        mainTable.append("<tr><td><strong>Nazwisko:</strong></td><td>" + list.persons.surname + "</td></tr>");
        mainTable.append("<tr><td><strong>Data urodzenia:</strong></td><td>" + list.persons.dateOfBirth + "</td></tr>");
        mainTable.append("<tr><td><strong>Numer telefonu:</strong></td><td>" + list.persons.phoneNumber + "</td></tr>");
        mainTable.append("<tr><td><strong>Adres e-mail:</strong></td><td>" + list.persons.mail + "</td></tr>");

        var detailTable = $("table.detailTable");
        detailTable.empty();
        detailTable.append("<tr><td><strong>Drugie imię:</strong></td><td>" + list.persons.secondName + "</td></tr>");
        detailTable.append("<tr><td><strong>Imię ojca:</strong></td><td>" + list.persons.fatherName + "</td></tr>");
        detailTable.append("<tr><td><strong>Imię i nazwisko matki:</strong></td><td>" + list.persons.motherName + " " + list.persons.motherSurname + "</td></tr>");
        detailTable.append("<tr><td><strong>Miejsce urodzenia:</strong></td><td>" + list.persons.birthPlace + "</td></tr>");
        detailTable.append("<tr><td><strong>Płeć:</strong></td><td>" + list.persons.sex + "</td></tr>");
        detailTable.append("<tr><td><strong>Wykształcenie:</strong></td><td>" + list.persons.education + "</td></tr>");
        detailTable.append("<tr><td><strong>Adres:</strong></td><td>" + list.persons.code + " " + list.persons.city + " " + list.persons.street + " " + list.persons.houseNumber + "</td></tr>");
        detailTable.append("<tr><td><strong>Rodzaj dokumentu tożsamości:</strong></td><td>" + list.persons.idType + "</td></tr>");
        detailTable.append("<tr><td><strong>Wydany przez:</strong></td><td>" + list.persons.idIssuedBy + "</td></tr>");
        detailTable.append("<tr><td><strong>Numer dowodu:</strong></td><td>" + list.persons.idNumber + "</td></tr>");
        detailTable.append("<tr><td><strong>Numer NIP:</strong></td><td>" + list.persons.taxpayerIdentificationNumber + "</td></tr>");
        detailTable.append("<tr><td><strong>Obywatelstwo:</strong></td><td>" + list.persons.nationality + "</td></tr>");
        detailTable.append("<tr><td><strong>Narodowość:</strong></td><td>" + list.persons.ethnicity + "</td></tr>");
    },
    counter: function () {
        var counterContent = $(".counter");
        counterContent.html("<span class='glyphicon glyphicon-user'>" + " " + list.personsCounter + "/" + list.totalPersonsNumber);
    }

};

$(document).ready(function () {
    list.init();
});